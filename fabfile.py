__author__ = 'marcellocardea'

"""Deploy script for AxantWeb

Make sure you run this with something like -i ~/.ssh/repo
to use the repo key for authentication
"""

APP_NAME = 'tgapp-stroller/sample-ecommerce' # This is the directory where the app is cloned
APP_ID = '513' # This is the USERID on the axantweb server
APP_REPOSITORY = 'https://bitbucket.org/axant/tgapp-stroller.git' # This is repository from where to clone on first deploy
APP_DEPLOY_BRANCH = 'master'
APP_SUPPORTS_MIGRATIONS = False
APP_USES_GEVENT = False # Keep in mind that enabling GEVENT requires requires WSGIApplicationGroup %{GLOBAL} in Apache VHost conf

import time
import fabric
from fabric.api import *
from fabric import operations
from fabric import colors
from fabric.contrib.console import confirm
from fabric.contrib.files import exists, append
import glob, os

APP_USER = 'aw%s' % APP_ID

env.hosts = ['%s@wsgi.axantweb.com' % APP_USER]

APP_PATH = os.path.join('/var/www', APP_ID, 'app', APP_NAME)
ENV_PATH = os.path.join('/var/www', APP_ID, 'penv')

ENV_ACTIVATE = os.path.join(ENV_PATH, 'bin/activate')

WSGI_APPLICATION = '''
from paste.deploy import loadapp

def init_application(path):
    APP_CONFIG = path + "/%s/staging.ini"
    application = loadapp("config:%%s" %% APP_CONFIG)
    return application
''' % APP_NAME

if APP_USES_GEVENT:
    WSGI_APPLICATION = '''import gevent
from gevent import monkey
monkey.patch_all()

''' + WSGI_APPLICATION


def _wait(seconds):
    for i in range(seconds, 0, -1):
        if i <= 2:
            i = colors.red('%s' % i)
        elif i <= 4:
            i = colors.yellow('%s' % i)
        print 'Starting in %s...' % i
        time.sleep(1.0)


def _cmd_in_venv(cmd):
    return 'source %s; %s' % (ENV_ACTIVATE, cmd)


def _first_deploy():
    # This is required to make gearbox available, otherwise only TurboGears gets installed
    run(_cmd_in_venv('pip install --use-mirrors "tg.devtools>=2.3.1"'))

    with cd('app'):
        run('git clone %s' % APP_REPOSITORY)
        run('rm wsgi_application.py')
        append('wsgi_application.py', WSGI_APPLICATION)


def deploy_dev():
    is_first_deploy = not exists(APP_PATH)
    if is_first_deploy:
        print 'Performing First deploy!'
        _first_deploy()

    with cd(APP_PATH):
        run('git pull')
        run('git checkout %s' % APP_DEPLOY_BRANCH)
        run(_cmd_in_venv('python setup.py develop'))
        with cd('../'):
            run(_cmd_in_venv('python setup.py develop'))




        if is_first_deploy:
            run(_cmd_in_venv('gearbox setup-app -c staging.ini'))

    if not APP_USES_GEVENT:
        run('touch run.wsgi')
    else:
        # GEVENT doesn't work with a plain touch, needs apache restart
        run('touch /tmp/please_restart_apache_in_5_minutes')
        print colors.yellow('USING GEVENT THE SERVER WILL TAKE UP TO 5 MINUTES TO RESTART')
        _wait(60*5)

