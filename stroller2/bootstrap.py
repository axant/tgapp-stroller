
# -*- coding: utf-8 -*-
"""Setup stroller2 application"""

from stroller2 import model
from tgext.pluggable import app_model

def bootstrap(command, conf, vars):
    print 'Bootstrapping stroller2...'
    managers_group = app_model.Group.query.find({'group_name': 'managers'}).first()
    p = app_model.Permission(permission_name='stroller2-admin', description='Permits to manage stroller2',
                             _groups=[managers_group._id])
    try:
        model.DBSession.add(p)
    except AttributeError:
        # mute ming complaints
        pass
    model.DBSession.flush()

