
var React = require('react/react'),

ReactDOM = require('react/react-dom-server');

var Item = React.createClass({
    getInitialState: function() {
        return {
            count: this.props.data.initialCount
        };
    },

    _increment: function() {
        this.setState({ count: this.state.count + 1 });
    },

    render: function() {
        return <div><a href="#" onClick={this._increment}>
            COUNT: {this.state.count}
        </a></div>
            ;
    }
});

ReactDOM.renderToString(<Item data={dukpy.data}/>, null);
