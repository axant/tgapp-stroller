'use strict';

var React = require('react/react'),
    ReactDOM = require('react/react-dom-server');

var Item = React.createClass({
    getInitialState: function getInitialState() {
        return {
            count: this.props.data.initialCount
        };
    },

    _increment: function _increment() {
        this.setState({ count: this.state.count + 1 });
    },

    render: function render() {
        return React.createElement(
            'div',
            null,
            React.createElement(
                'a',
                { href: '#', onClick: this._increment },
                'COUNT: ',
                this.state.count
            )
        );
    }
});

ReactDOM.renderToString(React.createElement(Item, { data: dukpy.data }), null);