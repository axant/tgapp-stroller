(function() {
  var cart, fetchCardBadge, updateCartBadge, writeCardBadge;

  cart = {};

  updateCartBadge = function(cartUrl, badgeEl) {
    if (typeof badgeEl === 'string') {
      badgeEl = [badgeEl];
      return fetchCardBadge(cartUrl, function(badgeStr) {
        return writeCardBadge(badgeStr, badgeEl);
      });
    }
  };

  fetchCardBadge = function(cartUrl, callback) {
    return $.getJSON(cartUrl, function(data) {
      cart = data;
      var badgeStr = 0;
      for (var sku in data.items){
          badgeStr += data.items[sku]
      }
      if (badgeStr === 0) {
        badgeStr = '';
      }
      return callback(badgeStr);
    });
  };

  writeCardBadge = function(badgeString, badgeEl) {
    return badgeEl.forEach(function(el, idx, arr) {
      var $el;
      $el = $(el);
      return $el.html(badgeString);
    });
  };

  this.updateCartBadge = updateCartBadge;

}).call(this);

