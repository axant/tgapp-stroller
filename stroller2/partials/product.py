from stroller2.lib import get_buy_product_form
from tg import expose
from tgext.pluggable import plug_url


@expose("genshi:stroller2.templates.partials.product.full_page")
def full_page(product):
    return dict(product=product, buy_form=get_buy_product_form(),
                buy_form_action=plug_url('stroller2', '/product/add_to_cart'))

@expose("genshi:stroller2.templates.partials.product.full_page")
def thumbnail(product):
    return dict(product=product, buy_form=get_buy_product_form(),
                buy_form_action=plug_url('stroller2', '/product/add_to_cart'))

@expose('genshi:stroller2.templates.partials.product.list_view')
def list_view(product):
    return dict(product=product)