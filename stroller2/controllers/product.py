# coding=utf-8
from __future__ import unicode_literals
from bson import ObjectId
from stroller2.lib import get_buy_product_form

from tg import expose, app_globals, abort, validate, request, require, TGController
import tg
from tg.flash import flash
from tg.i18n import lazy_ugettext as l_, ugettext as _
from tg.predicates import not_anonymous
from datetime import datetime
from tgext.ecommerce.lib.exceptions import CartLockedException
from tgext.pluggable import plug_url, plug_redirect


class ProductController(TGController):
    @expose('genshi:stroller2.templates.product.product')
    def _default(self, slug=None, product=None, *args, **kw):
        product_filter = dict(slug=slug, _id=product)

        identity = tg.request.identity
        if not (identity and 'managers' in identity['groups']):
            product_filter.update({'query': {'published': True}})

        product = app_globals.shop.product.get(**product_filter)
        if product is None:
            abort(404, 'Product not found')

        return dict(product=product)

    @require(not_anonymous())
    @expose()
    @validate(get_buy_product_form(), error_handler=_default)
    def add_to_cart(self, product=None, quantity=1, **kw):
        product = app_globals.shop.product.get(_id=product)
        if product is None:
            abort(404, 'Product not found')
        try:
            cart = app_globals.shop.cart.create_or_get(str(request.identity['user']._id))
        except CartLockedException:
            flash(_('The cart is unavailable, try again later'), 'error')
            return plug_redirect('stroller2', '/product/%s' % product.slug)
        if kw.get('action') == _('Add to cart'):
            if not app_globals.shop.product.buy(cart, product, 0, quantity):
                flash(_('The product is sold out'), 'error')
            else:
                flash(_('Product %s added to cart') % product.i18n_name)
        elif kw.get('action') == _('Buy now'):
            app_globals.shop.cart.drop(cart)
            if not app_globals.shop.product.buy(cart, product, 0, quantity):
                flash(_('The product is sold out'), 'error')
            else:
                plug_redirect('stroller2', '/cart/pay')

        return plug_redirect('stroller2', '/cart')

