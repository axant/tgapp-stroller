# coding=utf-8
from __future__ import unicode_literals
from stroller2.controllers.cart import CartController
from stroller2.controllers.newcart import NewCartController
from stroller2.controllers.product import ProductController
from stroller2.controllers.showcase import ShowcaseController
from stroller2.controllers.user_addresses import UserAddressesController

from tg import TGController, response
from tg import expose, flash, require, url, lurl, request, redirect, validate
from stroller2.controllers.manage.controller import ManageController


class RootController(TGController):
    manage = ManageController()
    showcase = ShowcaseController()
    product = ProductController()
    cart = CartController()
    user_addresses = UserAddressesController()
    newcart = NewCartController()

    # def _before(self, *args, **kw):
    #     response.headers['Access-Control-Allow-Origin'] = '*'
    #

    @expose('stroller2.templates.index')
    def index(self):
        return dict()
