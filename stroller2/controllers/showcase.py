# coding=utf-8
from __future__ import unicode_literals
from tg import TGController, expose, app_globals


class ShowcaseController(TGController):

    @expose('genshi:stroller2.templates.showcase.index')
    def index(self, **kw):
        products = app_globals.shop.product.get_many('product', query={'active': True}).all()
        return dict(products=products)

    # @expose('genshi:stroller2.templates.showcase.newindex')
    # def index(self, **kw):
    #     # products = app_globals.shop.product.get_many('product', query={'active': True}).all()
    #     # print("________PROD________",products)
    #     return dict()
    #     # return dict(items=cart.items, expires_at=cart.expires_at, value=cart.item_count,
    #     #             total=cart.total, tax=cart.tax, site_base_url_showcase=tg.url('/commerce/showcase', qualified=True),
    #     #             site_base_url_pay=tg.url('/cart/pay', qualified=True),
    #     #             subtotal=cart.subtotal) if cart else {}
    #
    # @expose('json')
    # def get_products(self, **kw):
    #     products = app_globals.shop.product.get_many('product', query={'active': True}).all()
    #     print("________PROD________",products)
    #     return dict(products=products)