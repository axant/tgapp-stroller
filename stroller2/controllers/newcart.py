import os

from bson import ObjectId

import tg
from tg import TGController, expose, predicates, app_globals, request, response
from stroller2.lib.predicates import UserHasCompiledAddresses
from tgext.ecommerce.lib.exceptions import CartLockedException

path_item= "/Users/maurobellinazzi/Axant/ecommerce/tgapp-stroller/stroller2/public/react_components/item.jsx"
item_file = open(os.path.join(os.path.dirname(__file__), path_item), 'r')

REACT_APP = item_file.read()

print REACT_APP,
#item_file.close()

class NewCartController(TGController):
    #allow_only = predicates.not_anonymous()

    @expose('genshi:stroller2.templates.cart.newindex')
    def index(self, **kw):
        return dict(react_app=REACT_APP, )

    @expose('json')
    def get_cart(self, **kw):
        user = request.identity['user']
        try:
            cart = app_globals.shop.cart.get(str(user._id))
        except CartLockedException:
            print 'locked'
            return {}
        print ' ritorno della roba', cart.items
        return dict(items=cart.items, expires_at=cart.expires_at, value=cart.item_count,
                    total=cart.total, tax=cart.tax, site_base_url_showcase=tg.url('/commerce/showcase', qualified=True),
                    site_base_url_pay=tg.url('/cart/pay', qualified=True),
                    subtotal=cart.subtotal) if cart else {}

    @expose('json')
    def delete(self, **kw):
        # kw= {'Name': 'Zara', 'Age': 7, 'Class': 'First'}

        print("___________QUI__________", kw)
        return kw


