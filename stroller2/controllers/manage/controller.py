# coding=utf-8
from __future__ import unicode_literals
from stroller2.controllers.manage.category import ManageCategoryController
from tg import TGController, expose, predicates
from stroller2.controllers.manage.product import ManageProductController

class ManageController(TGController):
    allow_only = predicates.has_permission('stroller2-admin')


    product = ManageProductController()
    category = ManageCategoryController()

    @expose()
    def index(self):
        return {}