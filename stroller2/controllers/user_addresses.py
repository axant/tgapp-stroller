# coding=utf-8
from __future__ import unicode_literals
from bson import ObjectId
from stroller2 import model
from stroller2.controllers.utils.temporary_photos import TemporaryPhotosUploader
from tg import TGController, expose, validate, request, redirect
from stroller2.lib import get_edit_shipping_address_form
import tg
from tg.flash import flash
from tgext.pluggable import plug_url
from tg.i18n import lazy_ugettext as l_, ugettext as _
from tg import predicates

class UserAddressesController(TGController):
    photos = TemporaryPhotosUploader()
    allow_only = predicates.not_anonymous()

    @expose('genshi:stroller2.templates.user_addresses.index')
    def index(self, **kw):
        user_id = tg.request.identity['user']._id
        addresses = model.UserAddresses.query.find({'user_id': user_id}).first() or {}
        return dict(shipping_address=getattr(addresses, 'shipping_address', {}))

    @expose('genshi:stroller2.templates.user_addresses.edit_shipping_address')
    def edit_shipping_address(self, **kw):
        user = request.identity['user']
        value = {'user_id': user._id}
        user_addresses = model.UserAddresses.query.find({'user_id': user._id}).first()
        if user_addresses:
            value.update(user_addresses.shipping_address)
        return dict(form=get_edit_shipping_address_form(), action=plug_url('stroller2', '/user_addresses/save_shipping_address'),
                    value=value)

    @expose()
    @validate(get_edit_shipping_address_form(), error_handler=edit_shipping_address)
    def save_shipping_address(self, **kw):
        user_id = ObjectId(kw.pop('user_id'))
        user_addresses = model.UserAddresses.query.find({'user_id': user_id}).first()
        if not user_addresses:
            model.UserAddresses(user_id=user_id, shipping_address=kw)
        else:
            user_addresses.shipping_address = kw
        flash(_('Shipping address updated'))
        return redirect(plug_url('stroller2', '/user_addresses/index'))