# coding=utf-8
from __future__ import unicode_literals

import json
import datetime
from stroller2 import model
from stroller2.lib.predicates import UserHasCompiledAddresses
from tg import expose, app_globals, request, predicates, tmpl_context, flash, TGController, redirect, require
from tg.i18n import ugettext as _
from tgext.ecommerce.lib.exceptions import CartLockedException, CartException
from tgext.pluggable import plug_redirect, plug_url

class CartController(TGController):
    allow_only = predicates.not_anonymous()

    def _before(self, *args, **kw):
        tmpl_context.in_cart = True

    @expose('genshi:stroller2.templates.cart.index')
    def index(self, **kw):
        return dict()

    @expose('genshi:stroller2.templates.cart.item')
    def cart_item(self, item, **kw):
        if isinstance(item, basestring):
            item = json.loads(item)
        return dict(item=item)

    @expose('json')
    def get_cart(self, **kw):
        user = request.identity['user']
        try:
            cart = app_globals.shop.cart.get(str(user._id))
        except CartLockedException:
            return

        return dict(items=cart.items, expires_at=cart.expires_at, value=cart.item_count,
                    total=cart.total, tax=cart.tax,
                    subtotal=cart.subtotal) if cart else {}

    @expose('json')
    def save_cart(self, model, **kw):
        user = request.identity['user']
        new_cart = json.loads(model)
        try:
            cart = app_globals.shop.cart.get(str(user._id))
        except CartLockedException:
            flash(_('The cart is temporary unavailable for routine maintenance, try again later'), 'error')
            return plug_redirect('stroller2', '/cart')
        for sku, item in new_cart['items'].iteritems():
            try:
                app_globals.shop.cart.update_item_qty(cart, sku, item['qty'])
            except CartException:
                flash(_('The cart is expired'), 'error')
                return plug_redirect('stroller2', '/cart')

        return dict(model=dict(items=cart.items, expires_at=cart.expires_at, value=cart.item_count,
                               total=cart.total, tax=cart.tax,
                               subtotal=cart.subtotal))

    @staticmethod
    def _cart_values(cart):
        if cart:
            return dict(total=cart.total, tax=cart.tax,
                        subtotal=cart.subtotal)

    @expose()
    @require(UserHasCompiledAddresses())
    def pay(self, **kw):
        user = request.identity['user']
        cart = app_globals.shop.cart.get(str(user._id))
        user_addresses = model.UserAddresses.query.find({'user_id': user._id}).first()

        if cart is None:
            flash(_('Your cart is expired'), 'error')
            plug_redirect('stroller2', '/cart')

        app_globals.shop.cart.update_order_info(cart, cart.total, shipment_info=user_addresses.shipping_address)
        redirect_url = app_globals.shop.pay(cart, plug_url('stroller2', '/cart/confirm_payment', qualified=True),
                                            plug_url('stroller2', '/cart/abort', qualified=True))
        return redirect(redirect_url)

    @expose()
    def abort(self, **kw):
        flash(_('There was an error with paypal, try again later'), 'error')
        plug_redirect('stroller2', '/cart')

    @expose()
    def confirm_payment(self, **kw):
        user = request.identity['user']
        cart = app_globals.shop.cart.get(str(user._id))
        if cart is None:
            flash(_('Your cart is expired'), 'error')
            plug_redirect('stroller2', '/cart')
        execute_url = app_globals.shop.confirm(cart, self.mount_point + '/execute', kw)
        return redirect(execute_url)


    @expose('json')
    def execute(self, **kw):
        user = request.identity['user']
        cart = app_globals.shop.cart.get(str(user._id))
        if cart is None:
            flash(_('Your cart is expired'), 'error')
            plug_redirect('stroller2', '/cart')

        if cart.order_info.payment.date <= cart.last_update:
            flash(_('The cart was modified, repeat buy process'), 'error')
            plug_redirect('stroller2', '/cart')

        response = app_globals.shop.execute(cart, kw)
        if response.get('result'):
            order = app_globals.shop.order.create(cart=cart, payment_date=datetime.datetime.utcnow(),
                                                  payer_info=response['payer_info'],
                                                  status='paid',
                                                  **cart.order_info.details)

            return {'status': 'success', 'order': order}
        else:
            plug_redirect('stroller2', '/cart/abort')


