# -*- coding: utf-8 -*-
"""The tgapp-stroller2 package"""
import os

import tg
from tg.configuration import config
from tgext.pluggable import plug
from tgext.webassets import Bundle


def plugme(app_config, options):
    app_config['_pluggable_stroller2_config'] = options
    tg.hooks.register('before_config', setup_webassets)

    return dict(appid='commerce', global_helpers=True, plug_bootstrap=True)


def setup_webassets(app):

    from webassets import Environment, Bundle
    assets_env = Environment(
        directory=os.path.join(os.path.dirname(__file__), 'public'),
        url='/_pluggable/stroller2',
    )

    from webassets.filter import register_filter
    from stroller2.lib.utils import JSXFilter

    register_filter(JSXFilter)

    assets_env.register('stroller_item', Bundle('react_components/item.jsx',
                                                      filters='jsx',
                                                      output='javascript/bundles/item.js'))

    config['tg.app_globals'].stroller_webassets = assets_env

    return app
