# coding=utf-8
from __future__ import unicode_literals
from bson import ObjectId
import tg
from tw2.core import ValidationError, Validator
from tg.i18n import lazy_ugettext as l_, ugettext as _


class UnicodeValidationError(ValidationError):
    def __str__(self):
        return unicode(self.msg)


class CurrentUserValidator(Validator):
    def _validate_python(self, value, state):
        super(CurrentUserValidator, self)._validate_python(value, state)
        if not value == str(tg.request.identity['user']._id):
            raise UnicodeValidationError(_('You can only edit your address'), value, state)


class ValidCategoryAncestorValidator(Validator):
    msgs = {'invalid_category_ancestor': 'The chosen parent for cateogry is invalid'}

    def __init__(self, category_id_field, parent_id_field, **kw):
        super(ValidCategoryAncestorValidator, self).__init__(**kw)
        self.category_id_field = category_id_field
        self.parent_id_field = parent_id_field

    def _validate_python(self, values, state=None):
        category_id = values.get(self.category_id_field)
        parent_id = values.get(self.parent_id_field)
        if parent_id:
            if category_id == parent_id:
                raise ValidationError('invalid_category_ancestor', self)

            parent = tg.app_globals.shop.category.get(parent_id)

            for ancestor in parent.ancestors:
                if category_id == str(ancestor['_id']):
                    raise ValidationError('invalid_category_ancestor', self)
