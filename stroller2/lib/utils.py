from tg.controllers.util import LazyUrl
# -*- coding: utf-8 -*-
from webassets.filter import Filter
import dukpy


class JSONLazyUrl(LazyUrl):
    def __json__(self):
        return str(self)


def json_lurl(base_url=None, params=None):
    return JSONLazyUrl(base_url, params)

class JSXFilter(Filter):
    name = 'jsx'
    max_debug_level = None

    def input(self, _in, out, **kw):
        src = dukpy.jsx_compile(_in.read())
        out.write(src)