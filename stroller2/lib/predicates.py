# coding=utf-8
from __future__ import unicode_literals
from stroller2 import model
from tg import request, flash
from tg.predicates import Predicate
from tg.i18n import ugettext as _
from tgext.pluggable import plug_redirect


class UserHasCompiledAddresses(Predicate):

    def __init__(self, **kw):
        super(UserHasCompiledAddresses, self).__init__(**kw)

    def evaluate(self, environ, credentials):
        user = request.identity['user']
        user_addresses = model.UserAddresses.query.find({'user_id': user._id}).first()
        if user_addresses is None:
            flash(_("You must compile shipping addresses before"), 'error')
            plug_redirect('stroller2', '/user_addresses/index')