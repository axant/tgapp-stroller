# -*- coding: utf-8 -*-

"""WebHelpers used in tgapp-stroller2."""

#from webhelpers import date, feedgenerator, html, number, misc, text
import dukpy
from tgext.ecommerce.model import Cart
from markupsafe import Markup
from tg import app_globals
from tgext.pluggable import plug_url
from tg.i18n import ugettext as _, lazy_ugettext as l_
import os

def bold(text):
    return Markup('<strong>%s</strong>' % text)


def stroller2_product_url(product):
    return plug_url('stroller2', '/product/%s' % product.slug)

def stroller2_product_share_url(product):
    return plug_url('stroller2', '/product/share/%s' % product.slug)

def item_remaing_quantity(item):
    return app_globals.shop.product.get(sku=item['sku']).configurations[0].qty

def items_total(item):
    return Cart.items_total(item)


def render_react(code, data):
    jsi = dukpy.JSInterpreter()
    requirements_folder = os.path.join(os.path.dirname(__file__), 'public', 'js_modules')
    jsi.loader.register_path(requirements_folder)

    jsx = dukpy.jsx_compile(code)
    return dukpy.evaljs(jsx, data=data)
